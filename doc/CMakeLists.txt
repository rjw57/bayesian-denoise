# LaTeX support
include(UseLATEX)

add_latex_document(
	statistical-denoising-paper.tex
	DEFAULT_PDF
	BIBFILES references.bib
	IMAGE_DIRS
		icons
		figures
		figures/cornell-317-272
		figures/cornell-240-180
		figures/cat
		figures/classroom
)
