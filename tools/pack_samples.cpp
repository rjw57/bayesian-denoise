#include <cmath>
#include <cstdlib>
#include <boost/program_options.hpp>
#include <error.h>
#include <fstream>
#include <iostream>
#include <stdexcept>

#include <sample.hpp>

using namespace denoise;
namespace po = boost::program_options;

namespace util
{

void denoise(const char* outfile, int64_t width, int64_t height, int64_t spp_target)
{
	int64_t n_samples(0);
	sample_image im(width, height);
	std::istream& is(std::cin);
	
	std::ofstream os(outfile);

	while(!is.eof())
	{
		float x, y, r, g, b;
		is >> x >> y >> r >> g >> b;

		if(is.eof())
			continue;

		int64_t ix(trunc(x)), iy(trunc(y));

		if((ix < 0) || (ix >= width))
		{
			std::cerr << "Skipping invalid x-co-ordinate: "
				<< ix << '\n';
			continue;
		}

		if((iy < 0) || (iy >= height))
		{
			std::cerr << "Skipping invalid y-co-ordinate: "
				<< iy << '\n';
			continue;
		}

		im(ix, iy).insert(sample(r,g,b));
		++n_samples;

		int64_t spp = n_samples / (width*height);

		// if we have a samples per pixel target, see it we've met it
		if((spp_target > 0) && (spp >= spp_target))
			break;

		if((n_samples & 0xfff) == 0)
		{
			std::cout << '\r' << "Read ";
			if(n_samples < 1000) {
				std::cout << n_samples;
			} else if(n_samples < 1000000) {
				std::cout << n_samples/1000 << "K";
			} else {
				std::cout << n_samples/1000000 << "M";
			}
			std::cout << " samples "
				<< "(approx. " << spp << " spp)        "
				<< std::flush;
		}
	}
	std::cout << '\n';

	std::cout << "Final sample count: " << n_samples << '\n';

	im.write(os);
	os.close();
}

}

int main(int argc, char **argv)
{
	po::options_description desc("Allowed options");
	desc.add_options()
		("help,h", "produce help message")
		("width,w", po::value<int>(), "image width")
		("height,e", po::value<int>(), "image height")
		("spp,s", po::value<int>()->default_value(0),
			"target samples per-pixel (0 = all samples in file)")
		("output,o", po::value<std::string>()->default_value("output.dat"),
		 	"output file name")
		;

	po::variables_map vm;

	try
	{
		po::store(po::parse_command_line(argc, argv, desc), vm);
		po::notify(vm);    

		if(vm.count("help"))
		{
			std::cout << desc << '\n';
			return EXIT_SUCCESS;
		}

		if((0 == vm.count("width")) || (0 == vm.count("height")))
		{
			error(EXIT_FAILURE, 0,
					"Must specify both --width and --height.");
		}

		util::denoise(vm["output"].as<std::string>().c_str(),
				vm["width"].as<int>(),
				vm["height"].as<int>(),
				vm["spp"].as<int>());

	}
	catch(std::exception& e)
	{
		error(EXIT_FAILURE, 0, "error: %s", e.what());
	}

	return EXIT_SUCCESS;
}
