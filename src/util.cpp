#include <cmath>
#include <png.h>
#include <stdint.h>
#include <stdexcept>

#include <util.hpp>

namespace denoise { namespace internal {

int write_rgb_bitmap_to_png(const uint8_t* pixels, int64_t w, int64_t h, const char *path)
{
    FILE * fp;
    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;
    int64_t y;
    png_byte ** row_pointers = NULL;

    /* "status" contains the return value of this function. At first
       it is set to a value which means 'failure'. When the routine
       has finished its work, it is set to a value which means
       'success'. */
    int status = -1;
    // int pixel_size = 3; /* RGB */
    int depth = 8; /* 8-bits per pixel */

    fp = fopen (path, "wb");
    if (! fp) {
        goto fopen_failed;
    }

    png_ptr = png_create_write_struct (PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (png_ptr == NULL) {
        goto png_create_write_struct_failed;
    }
    
    info_ptr = png_create_info_struct (png_ptr);
    if (info_ptr == NULL) {
        goto png_create_info_struct_failed;
    }
    
    /* Set up error handling. */

    if (setjmp (png_jmpbuf (png_ptr))) {
        goto png_failure;
    }
    
    /* Set image attributes. */

    png_set_IHDR (png_ptr,
                  info_ptr,
		  w, h, depth,
                  PNG_COLOR_TYPE_RGB,
                  PNG_INTERLACE_NONE,
                  PNG_COMPRESSION_TYPE_DEFAULT,
                  PNG_FILTER_TYPE_DEFAULT);
    
    /* Initialize rows of PNG. */

    row_pointers = reinterpret_cast<png_byte**>(png_malloc (png_ptr, h * sizeof (png_byte *)));
    for (y = 0; y < h; ++y) {
        row_pointers[h-y-1] = const_cast<png_byte*>(reinterpret_cast<const png_byte*>(pixels) + (y * w * 3));
    }
    
    /* Write the image data to "fp". */

    png_init_io (png_ptr, fp);
    png_set_rows (png_ptr, info_ptr, row_pointers);
    png_write_png (png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);

    /* The routine has successfully written the file, so we set
       "status" to a value which indicates success. */

    status = 0;
    
    png_free (png_ptr, row_pointers);
    
 png_failure:
 png_create_info_struct_failed:
    png_destroy_write_struct (&png_ptr, &info_ptr);
 png_create_write_struct_failed:
    fclose (fp);
 fopen_failed:
    return status;
}

float cm_pvalue(float cms, int64_t N1, int64_t N2)
{
	// table of the limiting distribution for the two-sample C-vM statistic
	// taken from Andersion, T.W.; Darling, D.A 
	//            "Asymptotic Theory of Certain 'Goodness of Fit' Criteria  Bases on Stochastic Processes"
	static const float cm2_z[] = {
		0.00000, 0.02480, 0.02878, 0.03177, 0.03430, 0.03656, 0.03865, 0.04061, 0.04247, 0.04427, 0.04601,
		0.04772, 0.04939, 0.05103, 0.05265, 0.05426, 0.05586, 0.05746, 0.05904, 0.06063, 0.06222, 0.06381,
		0.06541, 0.06702, 0.06863, 0.07025, 0.07189, 0.07354, 0.07521, 0.07690, 0.07860, 0.08032, 0.08206,
		0.08383, 0.08562, 0.08744, 0.08928, 0.09115, 0.09306, 0.09499, 0.09696, 0.09896, 0.10100, 0.10308,
		0.10520, 0.10736, 0.10956, 0.11182, 0.11412, 0.11647, 0.11888, 0.12134, 0.12387, 0.12646, 0.12911,
		0.13183, 0.13463, 0.13751, 0.14046, 0.14350, 0.14663, 0.14986, 0.15319, 0.15663, 0.16018, 0.16385,
		0.16765, 0.17159, 0.17568, 0.17992, 0.18433, 0.18892, 0.19371, 0.19870, 0.20392, 0.20939, 0.21512,
		0.22114, 0.22748, 0.23417, 0.24124, 0.24874, 0.25670, 0.26520, 0.27429, 0.28406, 0.29460, 0.30603,
		0.31849, 0.33217, 0.34730, 0.36421, 0.38331, 0.40520, 0.43077, 0.46136, 0.49929, 0.54885, 0.61981,
		0.74346, 1.16786,
	};

	static const int64_t n_cm2_z = sizeof(cm2_z) / sizeof(float);

	// compute parameters of the statistic's distribution
	int64_t N(N1+N2);
	float T_mean = (1.f/6.f) + (1.f/6.f) / static_cast<float>(N);
	float T_var  = (1.f/45.f) * (N+1.f)/static_cast<float>(N*N) * (4.f*N1*N2*N-3.f*(N1*N1+N2*N2)-2.f*N1*N2 )/(4.f*N1*N2);

	// translate the T statistic into the limiting distribution
	float CM_limiting_stat = (cms - T_mean) / sqrt(45.f*T_var) + (1.f/6.f);

	if(CM_limiting_stat > cm2_z[n_cm2_z-1])
	{
		return 1.f;
	}
	else if(CM_limiting_stat < cm2_z[0])
	{
		return 0.f;
	}
	else
	{
		// look for entries in cm2_z either side of the value
		int64_t upper_bound_idx = 1;
		while(cm2_z[upper_bound_idx] < CM_limiting_stat)
			++upper_bound_idx;

		// what p-value does this correspond to? (use linear interpolation)
		float lower_stat = cm2_z[upper_bound_idx-1];
		float upper_stat = cm2_z[upper_bound_idx];
		float lambda = (CM_limiting_stat - lower_stat) / (upper_stat - lower_stat);

		float lower_P = (upper_bound_idx - 1) * 0.01f;
		float upper_P = (upper_bound_idx == n_cm2_z-1) ? .999f : (upper_bound_idx * 0.01f);

		return lambda * (upper_P - lower_P) + lower_P;
	}

	throw std::runtime_error("not reached");
}

} }
