#include <boost/foreach.hpp>
#include <Eigen/Dense>
#include <iostream>

#include <sample.hpp>

namespace denoise {

void sample_bag::insert(const sample& s)
{
	if(s.is_zero())
	{
		++zero_sample_count_;
		return;
	}

	non_zero_samples_.push_back(s);
	cached_sum_ += s;
	cached_sum_sq_ += s.cwiseProduct(s);
}

std::ostream& sample_bag::write(std::ostream& os) const
{
	// write number of zero samples
	os.write(reinterpret_cast<const char*>(&zero_sample_count_), sizeof(int64_t));

	// write number of non-zero samples
	int64_t non_zero_sample_count_(non_zero_samples_.size());
	os.write(reinterpret_cast<const char*>(&non_zero_sample_count_), sizeof(int64_t));

	// write each sample
	for(sample_collection_t::const_iterator i(non_zero_samples_.begin()); i!=non_zero_samples_.end(); ++i)
	{
		float r(i->r()), g(i->g()), b(i->b());
		os.write(reinterpret_cast<const char*>(&r), sizeof(float));
		os.write(reinterpret_cast<const char*>(&g), sizeof(float));
		os.write(reinterpret_cast<const char*>(&b), sizeof(float));
	}

	return os;
}

sample sample_bag::mean() const
{
	Eigen::Vector3f mu = sum() / static_cast<float>(size());
	return mu;
}

sample sample_bag::variance() const
{
	Eigen::Vector3f mu2 = mean().cwiseProduct(mean());
	Eigen::Vector3f sq_mu = sum_sq() / static_cast<float>(size());
	return Eigen::Vector3f(sq_mu - mu2);
}

std::istream& sample_bag::read(std::istream& os)
{
	// read number of zero samples
	os.read(reinterpret_cast<char*>(&zero_sample_count_), sizeof(int64_t));

	// read number of non-zero samples
	int64_t non_zero_sample_count_;
	os.read(reinterpret_cast<char*>(&non_zero_sample_count_), sizeof(int64_t));

	non_zero_samples_.resize(non_zero_sample_count_);

	// read each sample
	cached_sum_.setZero();
	cached_sum_sq_.setZero();
	for(int64_t sample_idx=0; sample_idx < non_zero_sample_count_; ++sample_idx)
	{
		float r(0.f), g(0.f), b(0.f);
		os.read(reinterpret_cast<char*>(&r), sizeof(float));
		os.read(reinterpret_cast<char*>(&g), sizeof(float));
		os.read(reinterpret_cast<char*>(&b), sizeof(float));

		sample s(r,g,b);
		non_zero_samples_[sample_idx] = s;
		cached_sum_ += s;
		cached_sum_sq_ += s.cwiseProduct(s);
	}

	return os;
}

int64_t sample_image::size() const
{
	int64_t n_samples = 0;
	for(bag_collection_t::const_iterator i(bags.begin()); i!=bags.end(); ++i)
	{
		n_samples += i->size();
	}
	return n_samples;
}

std::ostream& sample_image::write(std::ostream& os) const
{
	os.write(reinterpret_cast<const char*>(&width), sizeof(int64_t));
	os.write(reinterpret_cast<const char*>(&height), sizeof(int64_t));

	for(bag_collection_t::const_iterator i(bags.begin()); i!=bags.end(); ++i)
	{
		i->write(os);
	}

	return os;
}

std::istream& sample_image::read(std::istream& os)
{
	os.read(reinterpret_cast<char*>(&width), sizeof(int64_t));
	os.read(reinterpret_cast<char*>(&height), sizeof(int64_t));

	bags.resize(width*height);

	for(int64_t idx=0; idx<width*height; ++idx)
	{
		bags[idx].read(os);
	}

	return os;
}

}
