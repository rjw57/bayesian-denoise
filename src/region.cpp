#include <algorithm>
#include <boost/foreach.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <cstdlib>
#include <Eigen/Dense>
#include <Eigen/LU>
#include <iomanip>
#include <iostream>
#include <iterator>
#include <limits>
#include <set>

#include <region.hpp>
#include <util.hpp>

using namespace Eigen;

namespace denoise {

inline float log_gamma_pdf(float k, float theta, float x)
{
	return (k-1.f)*log(x) - (x/theta) - lgamma(k) - k*log(theta);
}

inline float log_inverse_gamma_pdf(float alpha, float beta, float x)
{
	return alpha * log(beta) - lgammaf(alpha) + (-alpha-1.f)*log(x) - beta/x;
}

inline float log_laplace_pdf(float mu, float b, float x)
{
	return -log(2.f*b) - fabs(x-mu) / b;
}

inline float log_exponential_pdf(float lambda, float x)
{
	return log(lambda) - lambda * x;
}

std::istream& region_image::read_samples(std::istream& is)
{
	p_sample_image_->read(is);
	reset_regions();
	return is;
}

void region_image::set_sample_image(const sample_image_ptr& ps)
{
	p_sample_image_ = ps;
	reset_regions();
}

bool region_image::merge_region_neighbour(int64_t r_idx)
{
	if((r_idx < 0) || (r_idx >= size()))
		throw std::runtime_error("invalid region index");

	// is this neighbour stable?
	if(1 == neighbour_stable_set_.count(r_idx))
		return false; // ... yes, don't try again

	const region& r(*regions_[r_idx]);

	// extract neighbours
	std::vector<int64_t> neighbours;
	neighbours.reserve(r.neighbours().size());
	region_neighbours(r, std::back_inserter(neighbours));

	// randomise order
	std::random_shuffle(neighbours.begin(), neighbours.end());

	// try to merge neighbours
	BOOST_FOREACH(int64_t n_idx, neighbours)
	{
		// skip test if we know it'll fail
		if(index_pair_cache_.contains(
					internal::index_pair_cache::index_pair_type(r_idx, n_idx)))
			continue;

		const region& n(*regions_[n_idx]);

		// try a merge
		region_ptr merged(new region(r));
		merged->merge(n, *p_sample_image_);

		// should we merge?
		if(should_merge(r, n, *merged))
		{
			replace_regions(r_idx, n_idx, merged);
			return true;
		}

		// record the failure
		index_pair_cache_.insert(
				internal::index_pair_cache::index_pair_type(r_idx, n_idx));
	}

	// none of the neighbours were merged, record this in the neighbour stable set.
	neighbour_stable_set_.insert(r_idx);

	// since it is neighbour stable, we no-longer need cache entries for this region.
	index_pair_cache_.erase_index(r_idx);

	return false;
}

bool region_image::should_merge(const region& r1, const region& r2, const region& merged)
{
	if((r1.variance().y() == 0) && (r2.variance().y() == 0) && (r1.mean() == r2.mean()))
		return true;

	if((r1.allowed_size() == 0) || (r2.allowed_size() == 0))
		return false;

	// decision variable alpha = log(P(H_0|D) / P(H_1|D))
	float alpha = 0.f;

	// H0
	alpha += merged.log_lik() + merged.log_prior();

	// H1
	alpha -= r1.log_lik() + r1.log_prior();
	alpha -= r2.log_lik() + r2.log_prior();

	return alpha >= 0.f;
}

int64_t region_image::merge_regions(int64_t r1_idx, int64_t r2_idx)
{
	if((r1_idx < 0) || (r1_idx >= size()))
		throw std::runtime_error("r1 has invalid index");
	if((r2_idx < 0) || (r2_idx >= size()))
		throw std::runtime_error("r2 has invalid index");

	// merging the same region is invalid
	if(r1_idx == r2_idx)
		throw std::runtime_error("cannot merge a region with itself");

	// work out which direction we're merging regions
	int64_t to(std::min(r1_idx, r2_idx)), from(std::max(r1_idx, r2_idx));

	// create merged region
	region_ptr new_region(new region(*(regions_[to])));
	new_region->merge(*(regions_[from]), *p_sample_image_);
	return replace_regions(r1_idx, r2_idx, new_region);
}

int64_t region_image::replace_regions(int64_t r1_idx, int64_t r2_idx, region_ptr pr)
{
	if((r1_idx < 0) || (r1_idx >= size()))
		throw std::runtime_error("r1 has invalid index");
	if((r2_idx < 0) || (r2_idx >= size()))
		throw std::runtime_error("r2 has invalid index");

	// merging the same region is invalid
	if(r1_idx == r2_idx)
		throw std::runtime_error("cannot merge a region with itself");

	// remove this index pair from the cache
	index_pair_cache_.erase(internal::index_pair_cache::index_pair_type(r1_idx, r2_idx));

	// work out which direction we're merging regions
	int64_t to(std::min(r1_idx, r2_idx)), from(std::max(r1_idx, r2_idx));

	// update pixel map for from region
	BOOST_FOREACH(int64_t pixel_idx, regions_[from]->pixels())
	{
		pixel_regions_[pixel_idx] = to;
	}

	// replace
	regions_[to] = pr;
	regions_[from]->clear();

	// remove any reference to from in the cache
	index_pair_cache_.erase_index(from);

	// swap the from region with the last region
	regions_[from].swap(regions_.back());
	index_pair_cache_.reassign(regions_.size()-1, from);

	// fix up pixel_regions_
	BOOST_FOREACH(int64_t pixel_idx, regions_[from]->pixels())
	{
		pixel_regions_[pixel_idx] = from;
	}

	// fix the neighbour stable set, we don't care about the from region
	// any more since it's been merged, byt the region at the end of the
	// list took it's place. If that was neighbour stable, fixup the set.
	if(1 == neighbour_stable_set_.erase(regions_.size() - 1))
	{
		// the last region was neighbour stable, it's index has changed.
		neighbour_stable_set_.insert(from);
	}

	// the 'to' region is no-longer neighbour stable
	neighbour_stable_set_.erase(to);
	
	// truncate the regions list
	regions_.resize(regions_.size()-1);

	return to;
}

void region_image::reset_regions()
{
	// reset the regions
	regions_.clear();

	// we initially have one region per-pixel
	int64_t w(p_sample_image_->width), h(p_sample_image_->height);
	int64_t n_pixels = w * h;

	// we don't know if any of the regions are neighbour stable
	neighbour_stable_set_.clear();

	// we haven't tried to merge any regions
	index_pair_cache_.clear();

	// allocate space for n_pixels regions
	regions_.resize(n_pixels);
	pixel_regions_.resize(n_pixels);

	// for each pixel...
	for(int64_t pixel_idx = 0; pixel_idx < n_pixels; ++pixel_idx)
	{
		// set the corresponding region and pull a reference to it
		pixel_regions_[pixel_idx] = pixel_idx;

		// create the new region
		region_ptr pr(new region());
		regions_[pixel_idx] = pr;

		// assign this region the specified pixel
		pr->set_pixel(pixel_idx, *p_sample_image_);
	}
}

void region_image::region_neighbours(const region& r, std::set<int64_t>& out_indices) const
{
	out_indices.clear();

	// for each neighbour pixel, inset the region index into the set
	BOOST_FOREACH(int64_t pixel_idx, r.neighbours())
	{
		out_indices.insert(pixel_regions_[pixel_idx]);
	}
}

std::ostream& region_image::write_metrics(std::ostream& os)
{
	os << "n(regions) = " << std::setw(5) << regions_.size() << " ("
		<< std::setprecision(1) << std::fixed
		<< 0.1f * ((1000*regions_.size()) / (p_sample_image_->width*p_sample_image_->height))
		<< "%)";
	os << "; ";
	os << "n(n. stable) = " << std::setw(5) << neighbour_stable_set_.size();
	os << "; ";
	os << "n(decision cache) = " << std::setw(5) << index_pair_cache_.size();

	return os;
}

void region::clear()
{
	pixels_.clear();
	neighbours_.clear();
	model_.setZero();
	n_samples_ = 0;
	n_non_zero_samples_ = 0;
	n_allowed_samples_ = 0;
	log_likelihood_ = 0.f;
	log_prior_ = 0.f;
	mean_ = sample(0,0,0);
	variance_ = sample(0,0,0);
}

sample region::model_at(float x, float y, const sample_image& samples) const
{
	sample orig(samples(x,y).mean());

	// normalise the co-ordinates
	x /= static_cast<float>(samples.width);
	y /= static_cast<float>(samples.height);

	float model_y =
		model_(0) * x + model_(1) * x*x +
		model_(2) * y + model_(3) * y*y +
		model_(4) * x*y + model_(5);
	sample s;
	s.set_yuv(model_y, orig.u(), orig.v());
	return s;
}

void region::update_model(const sample_image& im)
{
	// We need at least 2 pixels to fit a plane[1]. With fewer than 2
	// pixels, we make some assumptions:
	//
	// [1] In the case of 2 pixels or a rank-deficient set of 3 pixels we
	// model the plane as being a constant gradient in the [x+y] direction.

	switch(pixels_.size())
	{
		case 0:
			// no pixels, just set the plane to zero
			model_.setZero();
			mean_ = sample(0,0,0);
			variance_ = sample(0,0,0);
			return;
			break; // unnecessary but good practise!
		case 1:
			// 1 pixel, just set constant component of plane
			mean_ = im(*(pixels_.begin())).mean();
			variance_ = sample(0,0,0);
			model_.setZero(); model_(5) = mean_.y();
			return;
			break; // unnecessary but good practise!
		default:
			// nop
			break;
	};

	// Our approach to fitting a plane is to fit the plane in a weighted
	// linear least squares fashion.
	//
	// We form the Nx3 matrix A specified as [ x x^2 y y^2 xy 1 ] where x and y are
	// the normalised x- and y- co-ordinates of the sample pixel co-ordinates.
	//
	// We then form the Nx1 vector b specified as the sample Y-values.
	//
	// The 3x1 plane matrix, m, is the linear least squares solution to A m = b.

	// Create A and b
	Matrix<float, Dynamic, 6> A;
	Matrix<float, Dynamic, 1> b;

	float im_width(static_cast<float>(im.width));
	float im_height(static_cast<float>(im.height));

	A.resize(pixels_.size(), 6);
	b.resize(pixels_.size(), 1);

	Vector3f all_sample_sum(0,0,0);
	Vector3f all_sample_sq_sum(0,0,0);

	// Iterate over all samples in the region
	int64_t insert_idx(0);
	float weight_sum(0.f);
	BOOST_FOREACH(int64_t pixel_idx, pixels_)
	{
		sample_image::sub_t pos(im.ind2sub(pixel_idx));
		const sample_bag& samples(im(pixel_idx));

		float norm_x(static_cast<float>(pos.first) / im_width);
		float norm_y(static_cast<float>(pos.second) / im_height);

		// The ideal weight is the reciprocal of the variance. each row
		// is scaled by the square-root of the weight (i.e. 1/std.
		// dev).

		float sample_y_mean = 0.f, sample_y_sq_mean = 0.f;
		BOOST_FOREACH(const sample& s, samples.non_zero_samples())
		{
			float y = s.y();
			sample_y_mean += y;
			sample_y_sq_mean += y*y;
		}
		sample_y_mean /= samples.size();
		sample_y_sq_mean /= samples.size();

		float sample_variance = sample_y_sq_mean - sample_y_mean*sample_y_mean;
		float weight = sample_variance > 0.f
			? 1.f / sqrt(sample_variance)
			: std::numeric_limits<float>::max();
		weight_sum += weight;

		A(insert_idx, 0) = weight * norm_x;
		A(insert_idx, 1) = weight * norm_x * norm_x;
		A(insert_idx, 2) = weight * norm_y;
		A(insert_idx, 3) = weight * norm_y * norm_y;
		A(insert_idx, 4) = weight * norm_x * norm_y;
		A(insert_idx, 5) = weight;
		b(insert_idx) = weight * sample_y_mean;

		insert_idx++;

		// update statistics
		all_sample_sum += weight * samples.mean();
		all_sample_sq_sum += weight * samples.mean().cwiseProduct(samples.mean());
	}

	if(static_cast<size_t>(insert_idx) != pixels_.size())
		throw std::runtime_error("sanity check failed");

	mean_ = Vector3f(all_sample_sum / weight_sum);
	Vector3f mean_sq = mean_.cwiseProduct(mean_);
	variance_ = Vector3f((all_sample_sq_sum / weight_sum) - mean_sq);

	// linear least-squares solve with weight vector
	typedef ColPivHouseholderQR<Matrix<float, Dynamic, 6> > solver6_t;
	typedef ColPivHouseholderQR<Matrix<float, Dynamic, 3> > solver3_t;
	typedef ColPivHouseholderQR<Matrix<float, Dynamic, 2> > solver2_t;

	solver6_t solver(A);
	if(solver.rank() >= 6)
	{
	  	// Solve for matrix M
	   	model_ = solver.solve(b);
	}
	else
	{
	  	// if we're of sufficient rank to fit a plane, do so.
	   	Matrix<float, Dynamic, 3> Aplane;
	    	Aplane.resize(A.rows(), 3);
	     	Aplane.leftCols<1>() = A.middleCols<1>(0);
	      	Aplane.middleCols<1>(1) = A.middleCols<1>(2);
	       	Aplane.rightCols<1>() = A.rightCols<1>();
		solver3_t solver_plane(Aplane);

		if(solver_plane.rank() >= 3)
		{
			Vector3f sub_plane(solver_plane.solve(b));
			model_.setZero();
			model_(0) = sub_plane(0);
			model_(2) = sub_plane(1);
			model_(5) = sub_plane(2);
		}
		else
		{
		  	// if we're of sufficient rank to fit a line along x+y, do so.
		   	Matrix<float, Dynamic, 2> Aline;
		    	Aline.resize(A.rows(), 2);
		     	Aline.leftCols<1>() = A.middleCols<1>(0) + A.middleCols<1>(2);
			Aline.rightCols<1>() = A.rightCols<1>();
		       	solver2_t solver_line(Aline);

			if(solver_line.rank() >= 2)
			{
			  	Vector2f sub_line(solver_line.solve(b));
			   	model_.setZero();
			    	model_(0) = sub_line(0);
			     	model_(2) = sub_line(0);
			      	model_(5) = sub_line(1);
			}
			else
			{
			  	// if all else fails, fit a constant
			   	model_.setZero();
			    	model_(5) = b.sum() / weight_sum;
			}
		}
	}
}

void region::update_probs(const sample_image& im)
{
	typedef std::vector<float> float_collection;
	
	n_allowed_samples_ = 0;
	log_likelihood_ = -std::numeric_limits<float>::max();
	log_prior_ = -std::numeric_limits<float>::max();

	// create a list of y-samples for the entire region
	float_collection region_y_samples;
	region_y_samples.reserve(n_non_zero_samples_);

	// find the minimum model value
	float model_lower_bound = std::numeric_limits<float>::max();
	BOOST_FOREACH(int64_t pixel_idx, pixels_)
	{
		sample_image::sub_t pos(im.ind2sub(pixel_idx));
		model_lower_bound = std::min(model_lower_bound,
				model_at(pos.first, pos.second, im).y());
	}

	float region_mean = 0.f;
	float region_sq_mean = 0.f;
	float min_expected_y = std::numeric_limits<float>::max();

	// calculate minimum expected y
	BOOST_FOREACH(int64_t pixel_idx, pixels_)
	{
		sample_image::sub_t pos(im.ind2sub(pixel_idx));
		float expected_y(model_at(pos.first, pos.second, im).y());
		min_expected_y = std::min(min_expected_y, expected_y);
	}

	// fill the samples
	BOOST_FOREACH(int64_t pixel_idx, pixels_)
	{
		// get the samples for this pixel
		sample_image::sub_t pos(im.ind2sub(pixel_idx));
		const sample_bag& samples(im(pixel_idx));

		// work out what we expect the value to be
		float expected_y(model_at(pos.first, pos.second, im).y());

		float sample_scale = samples.non_zero_samples().size() /
			static_cast<float>(samples.size());

		// non-zero samples
		BOOST_FOREACH(const sample& s, samples.non_zero_samples())
		{
			float y = s.y() * sample_scale;
			//if(y < min_expected_y)
			//	continue;
			float v = y - expected_y;
			region_y_samples.push_back(v);
			region_mean += v;
			region_sq_mean += v*v;
		}
	}

	region_mean /= region_y_samples.size();
	region_sq_mean /= region_y_samples.size();

	// sort the samples
	std::sort(region_y_samples.begin(), region_y_samples.end());

	n_allowed_samples_ = region_y_samples.size();
	if(n_allowed_samples_ == 0)
	{
		// if there are no samples for the region, the likelihood is 1 but the prior is 0.
		log_likelihood_ = 0.f;
		log_prior_ = -std::numeric_limits<float>::max();
		return;
	}

	// find the per-pixel samples
	log_likelihood_ = 0.f;
	log_prior_ = 0.f;
	BOOST_FOREACH(int64_t pixel_idx, pixels_)
	{
		// get the samples for this pixel
		sample_image::sub_t pos(im.ind2sub(pixel_idx));
		const sample_bag& samples(im(pixel_idx));
		float_collection pixel_y_samples;
		pixel_y_samples.reserve(samples.non_zero_samples().size());

		float pixel_mean = 0.f;
		float pixel_sq_mean = 0.f;
		
		// work out what we expect the value to be
		float expected_y(model_at(pos.first, pos.second, im).y());

		float sample_scale = samples.non_zero_samples().size() /
			static_cast<float>(samples.size());

		// non-zero samples
		BOOST_FOREACH(const sample& s, samples.non_zero_samples())
		{
			float y = s.y() * sample_scale;
			//if(y < min_expected_y)
			//	continue;
			float v = y - expected_y;
			pixel_y_samples.push_back(v);
			pixel_mean += v;
			pixel_sq_mean += v*v;
		}

		pixel_mean /= pixel_y_samples.size();
		pixel_sq_mean /= pixel_y_samples.size();

		if(pixel_y_samples.size() == 0)
		{
			log_likelihood_ = -std::numeric_limits<float>::max();
			log_prior_ = -std::numeric_limits<float>::max();
			return;
		}

		// sort the samples
		std::sort(pixel_y_samples.begin(), pixel_y_samples.end());

		// compute the C-vM p-value
		float cm_stat = internal::cm_statistic_when_sorted(
				pixel_y_samples.begin(), pixel_y_samples.end(),
				region_y_samples.begin(), region_y_samples.end());
		float cm_pval = internal::cm_pvalue(cm_stat,
				pixel_y_samples.size(), region_y_samples.size());

		// update the log-likelihood
		log_likelihood_ += log(1.f - cm_pval);

		// float n_p(pixel_y_samples.size()), n_r(region_y_samples.size());
		// log_prior_ += log_exponential_pdf(1.f, sqrt((n_r+n_p)/(n_r*n_p)));

		// our prior belief in this pixel is related to the difference
		// of the mean and the expected value. The variance of the mean is 
		// expected to be the variance / N samples.
		// 
		// The variance of a laplace dist. is 2b^2 => b = sqrt(0.5*variance) = sqrt(0.5*variance/N)
		//float pixel_variance = pixel_sq_mean - pixel_mean*pixel_mean;
		//log_prior_ += log_laplace_pdf(0.f,
		//		sqrt(0.5f * pixel_variance / pixel_y_samples.size()), pixel_mean);
	}

	// want populous regions
	log_prior_ += log_inverse_gamma_pdf(1.f, 1.f, region_y_samples.size());

	// want big regions
	//log_prior_ += log_inverse_gamma_pdf(1.f, 1.f, pixels_.size());

	// want low residual std. deviation
	// float region_variance = region_sq_mean - region_mean*region_mean;
	// log_prior_ += log_exponential_pdf(1.f, sqrt(region_variance));

	// want regions with low residual mean
	// log_prior_ += log_laplace_pdf(0.f, 1.f, region_mean);
}

void region::set_pixel(int64_t idx, const sample_image& im)
{
	// reset this region
	clear();

	// add this pixel to the region
	pixels_.insert(idx);

	// update the sample cache count
	n_samples_ = im(idx).size();
	n_non_zero_samples_ = im(idx).non_zero_samples().size();

	// extract image width and height
	int64_t w(im.width), h(im.height);

	// look for neighbours
	sample_image::sub_t p(im.ind2sub(idx));
	if(p.first > 0) {
		neighbours_.insert(im.sub2ind(p.first-1, p.second));
	}
	if(p.first < (w-1)) {
		neighbours_.insert(im.sub2ind(p.first+1, p.second));
	}
	if(p.second > 0) {
		neighbours_.insert(im.sub2ind(p.first, p.second-1));
	}
	if(p.second < (h-1)) {
		neighbours_.insert(im.sub2ind(p.first, p.second+1));
	}

	update_all(im);
}

void region::merge(const region& r, const sample_image& im)
{
	// compute a merged set of pixels == union of pixels
	BOOST_FOREACH(int64_t idx, r.pixels_)
	{
		pixels_.insert(idx);
	}

	// update sample count
	n_samples_ += r.n_samples_;
	n_non_zero_samples_ += r.n_non_zero_samples_;

	// compute a merged set of neighbours
	BOOST_FOREACH(int64_t idx, r.neighbours_)
	{
		neighbours_.insert(idx);
	}

	// these new neighbours must not include any of our new pixels
	std::deque<int64_t> new_neighbours;
	std::set_difference(neighbours_.begin(), neighbours_.end(),
			pixels_.begin(), pixels_.end(),
			std::back_inserter(new_neighbours));

	neighbours_.clear();
	BOOST_FOREACH(int64_t idx, new_neighbours)
	{
		neighbours_.insert(idx);
	}

	update_all(im);
}

namespace internal
{

void index_pair_cache::insert(const index_pair_type& p)
{
	int64_t min_idx(std::min(p.first, p.second));
	int64_t max_idx(std::max(p.first, p.second));
	index_map_[min_idx].insert(max_idx);
}

bool index_pair_cache::contains(const index_pair_type& p) const
{
	int64_t min_idx(std::min(p.first, p.second));
	int64_t max_idx(std::max(p.first, p.second));
	index_map_type::const_iterator set(index_map_.find(min_idx));
	if(set == index_map_.end())
		return false;
	return (1 == set->second.count(max_idx));
}

void index_pair_cache::erase(const index_pair_type& p)
{
	int64_t min_idx(std::min(p.first, p.second));
	int64_t max_idx(std::max(p.first, p.second));

	index_map_type::iterator set(index_map_.find(min_idx));
	if(set == index_map_.end())
		return;
	set->second.erase(max_idx);
}

void index_pair_cache::erase_index(int64_t idx)
{
	// erase all pairs where idx is the minimum
	index_map_.erase(idx);

	// clear up all other entries
	BOOST_FOREACH(index_map_type::value_type& idx_set_pair, index_map_)
	{
		if(idx_set_pair.first < idx)
			idx_set_pair.second.erase(idx);
	}
}

void index_pair_cache::reassign(int64_t from_idx, int64_t to_idx)
{
	// this will store the set of indices we need to fix up
	std::deque<int64_t> entries_to_fix;

	index_map_type::const_iterator set(index_map_.find(from_idx));
	if(set != index_map_.end())
	{
		// handle the re-assigment of from -> to where from was the minimum index.
		std::copy(set->second.begin(), set->second.end(),
				std::back_inserter(entries_to_fix));
		index_map_.erase(from_idx);
	}

	// look for all remaining entries to fix
	BOOST_FOREACH(index_map_type::value_type& idx_set_pair, index_map_)
	{
		if(1 == idx_set_pair.second.erase(from_idx))
			entries_to_fix.push_back(idx_set_pair.first);
	}

	BOOST_FOREACH(int64_t idx, entries_to_fix)
	{
		insert(index_pair_type(idx, to_idx));
	}
}

void index_pair_cache::clear()
{
	index_map_.clear();
}

int64_t index_pair_cache::size() const
{
	int64_t s(0);

	BOOST_FOREACH(const index_map_type::value_type& idx_set_pair, index_map_)
	{
		s += idx_set_pair.second.size();
	}

	return s;
}

}

int write_region_map_png(const region_image& im, const char* filename)
{
	const region_image::sample_image_ptr& samples(im.ptr_to_sample_image());
	int64_t w(samples->width), h(samples->height);
	std::vector<uint8_t> pixels(w*h*3);

	// we use a Mersenne twister to pseudo-randomly modify the region index for display
	boost::random::mt19937 gen;
	boost::random::uniform_int_distribution<> dist(0, 63); // 6-bits
	for(int64_t pixel_idx = 0; pixel_idx < w*h; ++pixel_idx)
	{
		gen.seed(im.region_index_for_pixel(pixel_idx));
		int32_t region_bits = dist(gen);
		pixels[3*pixel_idx] = (region_bits & 0x3) << 6;
		pixels[3*pixel_idx + 1] = ((region_bits >> 2) & 0x3) << 6;
		pixels[3*pixel_idx + 2] = ((region_bits >> 4) & 0x3) << 6;
	}

	return internal::write_rgb_bitmap_to_png(pixels.data(), w, h, filename);
}

int write_region_model_png(const region_image& im, const char* filename)
{
	const region_image::sample_image_ptr& samples(im.ptr_to_sample_image());
	int64_t w(samples->width), h(samples->height);
	std::vector<uint8_t> pixels(w*h*3);

	for(int64_t pixel_idx = 0; pixel_idx < w*h; ++pixel_idx)
	{
		const region& reg(im.region_for_pixel(pixel_idx));
		sample_image::sub_t pos(samples->ind2sub(pixel_idx));
		sample model_value(reg.model_at(pos.first, pos.second, *(im.ptr_to_sample_image())));

		// poor man's gamma correction
		model_value = Vector3f(model_value
				.cwiseMax(Vector3f(0, 0, 0))
				.cwiseMin(Vector3f(1, 1, 1))
				.cwiseSqrt());

		pixels[3*pixel_idx] = static_cast<uint8_t>(255.f * model_value.r());
		pixels[3*pixel_idx + 1] = static_cast<uint8_t>(255.f * model_value.g());
		pixels[3*pixel_idx + 2] = static_cast<uint8_t>(255.f * model_value.b());
	}

	return internal::write_rgb_bitmap_to_png(pixels.data(), w, h, filename);
}

}
