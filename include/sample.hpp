#ifndef __DENOISE_SAMPLE_HPP
#define __DENOISE_SAMPLE_HPP

#include <boost/utility.hpp>
#include <cstdlib>
#include <deque>
#include <Eigen/Dense>
#include <iostream>
#include <stdexcept>
#include <utility>

namespace denoise {

class sample : public Eigen::Vector3f
{
	public:

	sample() : Eigen::Vector3f(0,0,0) { }
	sample(float r, float g, float b) : Eigen::Vector3f(r, g, b) { }
	sample(const sample& s) : Eigen::Vector3f(s) { }
	sample(const Eigen::Vector3f& v) : Eigen::Vector3f(v) { }

	// convenience accessors
	const float& r() const { return coeffRef(0); }
	const float& g() const { return coeffRef(1); }
	const float& b() const { return coeffRef(2); }

	void set_rgb(float r_, float g_, float b_) { *this = Eigen::Vector3f(r_, g_, b_); } 

	// zero-checking
	bool is_zero() const { return (r()==0.f) && (g()==0.f) && (b()==0.f); }

	// sample luminance and chrominance
	float y() const { return wr*r() + wg*g() + wb*b(); }
	float u() const { return umax * (b()-y()) / (1.f-wb); }
	float v() const { return vmax * (r()-y()) / (1.f-wr); }

	// set via YUV
	void set_yuv(float y, float u, float v)
	{
		float r_ = y + v * (1.f-wr) / vmax;
		float g_ = y - u * ((wb*(1.f-wb)) / (umax*wg)) - v * ((wr*(1.f-wr)) / (vmax*wg));
		float b_ = y + u * (1.f-wb) / umax;
		set_rgb(r_, g_, b_);
	}

	protected:

	// see http://en.wikipedia.org/wiki/YUV
	static const float wr = 0.299f;
	static const float wg = 0.587f;
	static const float wb = 0.114f;
	static const float umax = 0.436f;
	static const float vmax = 0.615f;

	public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

class sample_bag
{
	public:

	typedef std::deque<sample> sample_collection_t;

	sample_bag() : zero_sample_count_(0), cached_sum_(0,0,0), cached_sum_sq_(0,0,0) { }

	int64_t size() const { return zero_sample_count_ + non_zero_samples_.size(); }

	void insert(const sample& s);

	const sample_collection_t& non_zero_samples() const { return non_zero_samples_; }

	sample mean() const;
	sample variance() const;
	sample std() const { return Eigen::Vector3f(variance().cwiseSqrt()); }

	const sample& sum() const { return cached_sum_; }
	const sample& sum_sq() const { return cached_sum_sq_; }

	std::istream& read(std::istream& os);
	std::ostream& write(std::ostream& os) const;

	protected:

	int64_t             zero_sample_count_;
	sample_collection_t non_zero_samples_;
	sample              cached_sum_;
	sample              cached_sum_sq_;

	public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

struct sample_image : protected boost::noncopyable
{
	typedef std::deque<sample_bag> bag_collection_t;
	typedef std::pair<int64_t, int64_t> sub_t;

	int64_t width, height;
	bag_collection_t bags;

	sample_image() : width(0), height(0) { }
	sample_image(int64_t w, int64_t h) : width(w), height(h), bags(w*h) { }

	const sample_bag& operator () (const sub_t& s) const
		{ chk_sub(s); return bags[sub2ind(s)]; }
	sample_bag& operator () (const sub_t& s)
		{ chk_sub(s); return bags[sub2ind(s)]; }

	const sample_bag& operator () (int64_t x, int64_t y) const
		{ chk_sub(sub_t(x,y)); return bags[sub2ind(x,y)]; }
	sample_bag& operator () (int64_t x, int64_t y)
		{ chk_sub(sub_t(x,y)); return bags[sub2ind(x,y)]; }

	const sample_bag& operator () (int64_t i) const
		{ chk_ind(i); return bags[i]; }
	sample_bag& operator () (int64_t i)
		{ chk_ind(i); return bags[i]; }

	int64_t sub2ind(int64_t x, int64_t y) const
		{ chk_sub(sub_t(x,y)); return x + y*width; }
	int64_t sub2ind(const sub_t& s) const
		{ chk_sub(s); return sub2ind(s.first, s.second); }
	sub_t ind2sub(int64_t ind) const
		{ chk_ind(ind); return sub_t(ind % width, ind / width); }

	int64_t size() const;

	std::ostream& write(std::ostream& os) const;
	std::istream& read(std::istream& os);

	protected:

	void chk_ind(int64_t i) const
	{
		if((i < 0) || (i >= width*height))
			throw std::runtime_error("invalid index");
	}

	void chk_sub(sub_t s) const
	{
		if((s.first < 0) || (s.first >= width) ||
				(s.second < 0) || (s.second >= height))
			throw std::runtime_error("invalid subscript");
	}

	public:

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

}

#endif // __DENOISE_SAMPLE_HPP
